# Simple text editor

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Initial setup

Run `npm install` in order to setup application

## Development server

Run `npm start` for a dev server.

## To run tests:

Run `npm test` to run jest tests (added one example of a snaphot test)

## Note

Text example is loaded from sample file, but also the text can be modified as in any editor: inserted or typed in.
