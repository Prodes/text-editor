import React, { PureComponent } from "react";
import "./ActionButton.css";

class ActionButton extends PureComponent {
  render() {
    const { tagName, selected, clickFormatButtonAction } = this.props;
    return (
      <button
        className={`format-action ${selected ? "selected" : ""}`}
        type="button"
        onClick={clickFormatButtonAction.bind(null, tagName)}
      >
        <b>{tagName.toUpperCase()}</b>
      </button>
    );
  }
}

export default ActionButton;
