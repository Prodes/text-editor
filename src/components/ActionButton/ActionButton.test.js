import React from "react";
import ActionButton from ".";
import renderer from "react-test-renderer";
import { boldTag } from "../../constants";

it("renders correctly", () => {
  const appliedTags = [];
  const clickFormatButtonActionMock = jest.fn();

  const tree = renderer
    .create(
      <ActionButton
        key={boldTag}
        tagName={boldTag}
        selected={appliedTags.includes(boldTag)}
        clickFormatButtonAction={clickFormatButtonActionMock}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
