import React, { Component } from "react";
import ControlPanel from "./containers/ControlPanel";
import FileZone from "./containers/FileZone";
import getMockText from "./text.service";
import { fetchSynonyms } from "./api";
import { boldTag, italicTag, underlineTag } from "./constants";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedText: "",
      appliedTags: [],
      synonyms: []
    };
    this.changeSelectionAction = this.changeSelectionAction.bind(this);
    this.clickFormatButtonAction = this.clickFormatButtonAction.bind(this);
  }

  componentDidMount() {
    this.getText();
  }

  render() {
    const { synonyms, appliedTags } = this.state;
    return (
      <div className="App">
        <header>
          <span>Simple Text Editor</span>
        </header>
        <main>
          <ControlPanel
            synonyms={synonyms}
            appliedTags={appliedTags}
            clickFormatButtonAction={this.clickFormatButtonAction}
          />
          <FileZone changeSelectionAction={this.changeSelectionAction} />
        </main>
      </div>
    );
  }

  getText() {
    getMockText().then(function(text) {
      const editor = document.getElementById("file");
      if (editor) {
        editor.textContent = text;
      }
    });
  }

  changeSelectionAction(text, parentTags) {
    this.setState({ appliedTags: parentTags });
    fetchSynonyms(text).then(data => {
      const synonyms = data.map(d => d.word);
      this.setState({ synonyms });
    });
    return;
  }

  clickFormatButtonAction(buttonName, state) {
    const { appliedTags } = this.state;
    let newTags = [];
    if (appliedTags.includes(buttonName)) {
      // disable button
      newTags = appliedTags.filter(t => t !== buttonName);
    } else {
      newTags = newTags.concat(appliedTags, buttonName);
    }

    let command = "";
    switch (buttonName) {
      case boldTag:
        command = "bold";
        break;
      case italicTag:
        command = "italic";
        break;
      case underlineTag:
        command = "underline";
        break;
      default:
        command = "";
    }
    if (command) {
      document.execCommand(command, false, null);
    }
    this.setState({ appliedTags: newTags });
  }
}

export default App;
