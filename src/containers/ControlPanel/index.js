import React, { Component } from "react";
import { boldTag, italicTag, underlineTag } from "../../constants";
import ActionButton from "../../components/ActionButton";
import "./ControlPanel.css";

class ControlPanel extends Component {
  render() {
    const { synonyms, appliedTags, clickFormatButtonAction } = this.props;
    const ACTION_BUTTONS = [boldTag, italicTag, underlineTag];
    return (
      <div id="control-panel">
        <div id="format-actions">
          {/* Now we can have array of actions/tags and add as many action buttons as we need */}
          {ACTION_BUTTONS.map(actionTag => (
            <ActionButton
              key={actionTag}
              tagName={actionTag}
              selected={appliedTags.includes(actionTag)}
              clickFormatButtonAction={clickFormatButtonAction}
            />
          ))}

          {/* Example of colorizing button */}
          <button
            className={`format-action`}
            type="button"
            onClick={this.changeColorClickHandler}
          >
            <b style={{ color: "red" }}>Red Font</b>
          </button>

          {/* Show dropdown only when there are synonyms available  */}
          {synonyms && synonyms.length > 0 && (
            <select
              onChange={this.selectSynonymHandler}
              value=""
              placeholder="Select synonym..."
            >
              <option value="" disabled hidden>
                Select synonym...
              </option>
              {synonyms.map(synonym => (
                <option key={synonym}>{synonym}</option>
              ))}
            </select>
          )}
        </div>
      </div>
    );
  }

  changeColorClickHandler() {
    document.execCommand("ForeColor", false, "#FF0000");
  }

  selectSynonymHandler(e) {
    const selectedSynonym = e.target.value;
    let sel, range;
    if (window.getSelection) {
      sel = window.getSelection();
      if (sel.rangeCount) {
        range = sel.getRangeAt(0);
        range.deleteContents();
        range.insertNode(document.createTextNode(selectedSynonym));
      }
    } else if (document.selection && document.selection.createRange) {
      range = document.selection.createRange();
      range.text = selectedSynonym;
    }
  }
}

export default ControlPanel;
