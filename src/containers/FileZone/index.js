import React, { Component } from "react";
import "./FileZone.css";

class FileZone extends Component {
  constructor(props) {
    super(props);
    this.editorMouseUpHandler = this.editorMouseUpHandler.bind(this);
  }
  render() {
    return (
      <div id="file-zone">
        <div
          id="file"
          contentEditable="true"
          onMouseUp={this.editorMouseUpHandler}
        ></div>
      </div>
    );
  }

  editorMouseUpHandler() {
    const selection = window.getSelection();
    const text = selection.toString();
    if (text !== "") {
      const { changeSelectionAction } = this.props;
      // get parent nodes
      let parentNode = selection.anchorNode.parentNode;
      let parents = [];
      while (parentNode) {
        const tagName = parentNode.tagName.toLowerCase();
        if (!tagName || tagName === "div") {
          break;
        } else {
          parents.push(tagName);
          parentNode = parentNode.parentNode;
        }
      }

      changeSelectionAction(text, parents);
    }
  }
}

export default FileZone;
