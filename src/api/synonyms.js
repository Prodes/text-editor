export const fetchSynonyms = word => {
  return fetch(`https://api.datamuse.com/words?ml=${word}`).then(resp =>
    resp.json()
  );
  // .then(data => console.log(data));
};
